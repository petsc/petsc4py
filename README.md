PETSc for Python
================

**WARNING**: This is no longer the official repository of petsc4py.

**WARNING**: All petsc4py development is now done within the official [petsc.git](https://gitlab.com/petsc/petsc) repository, see [petsc.git/src/bindings/petsc4py](https://gitlab.com/petsc/petsc/-/tree/master/src/binding/petsc4py).

A new [petsc4py.git](https://gitlab.com/petsc/petsc4py) repository is hosted at GitLab and it is synchronized at release points using `git-subtree` with petsc4py sources from [src/bindings/petsc4py](https://gitlab.com/petsc/petsc/-/tree/master/src/binding/petsc4py). **DO NOT SUBMIT MERGE REQUEST** to [petsc4py.git](https://gitlab.com/petsc/petsc4py), use the [petsc.git](https://gitlab.com/petsc/petsc) repository instead.

This repository clone at Bitbucket contains petc4py commits up to **Jul 21, 2020**.

To update your local repository to use the new location, you need to do

```
git remote set-url origin git@gitlab.com:petsc/petsc4py.git
```

or

```
git remote set-url origin https://gitlab.com/petsc/petsc4py.git
```
